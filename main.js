var taskNumber; // Número total de tareas que se rellena en la primera recarga de la tabla `reloadTable(...)`
var limit = 5; // Número que indica el límite en la paginación de la tabla
toastr.options = {
    "closeButton": true,
    "timeOut": "3000"
}; // Declaramos un toastr para que salga el botón de cerrar y la transición dure 3 segundos

$(document).ready(function () {
    reloadTable(); //Recargar tabla

    loadCategories(); //Cargar checks de categorías

    $("#btnSave").on("click", function () {
        addTask();
    }); // Click del botón de guardado

    $("#btnTableBack").on("click", function () {
        limit = limit - 5;
        if (limit > 0) {
            reloadTable(limit);
        } else {
            limit = 5;
        }
    }); // Click del botón atrás en la paginación de la tabla

    $("#btnTableNext").on("click", function () {
        limit = limit + 5;
        if (limit >= (taskNumber + 5)) {
            limit = limit - 5;
            return;
        }
        reloadTable(limit);
    }); // Click del botón siguiente en la paginación de la tabla

    $('#table').off('click', '.js-btnEdit').on('click', '.js-btnEdit', function (e) {
        var id = $(this).data("id");
        removeTask(id);
    }); //Click del botón de eliminar tarea
});

function removeTask(id) {
    // Eliminar tarea
    swal({
        title: "¿Estás seguro?",
        text: "Vas a eliminar la tarea",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
        .then((willDelete) => {
        if (willDelete) { // Si el usuario indica que sí quiere eliminar la tarea
            $.ajax({
                    url: 'controller/task/delete.php',
                    type: 'POST',
                    data: {"id": id},
                    success: function (resp) {
                        reloadTable();
                        document.getElementById("taskForm").reset();
                        limit = 5;
                        toastr.success('Tarea eliminada correctamente', 'Éxito');
                    },
                    error: function (err) {
                        swal("Error!", err.responseJSON.message, "error");
                    }
                });
            } else {
                swal("Tranquilo, no se ha eliminado");
            }
        });
}

function addTask() {
    // Añadir tarea
    // Comprobar si el campo `Nombre de tarea` está vacio, por si la comprobación HTML falla
    if ($("#taskName").val() != "") {
        var form = $('#taskForm');
        $.ajax({
            url: 'controller/task/create.php',
            type: 'POST',
            data: form.serialize(),
            success: function (resp) {
                document.getElementById("taskForm").reset();
                reloadTable();
                limit = 5; // Restablecer limite ya que se ha recargado la tabla y empieza de nuevo en 0
                toastr.success('Tarea guardada correctamente', 'Éxito');
            },
            error: function (err) {
                swal("Error!", err.responseJSON.message, "error");
            }
        });
    }
}

function reloadTable(limit = 5) {
    //Recargar tabla
    $.ajax({
        url: 'controller/task/get.php',
        data: {"limit": limit},
        type: 'POST',
        success: function (resp) {
            taskNumber = resp['taskNumber']; // Actualizar variable de total de tareas
            // Actualizar pie de tabla
            $("#showingRegisters").html('Mostrando del ' + (limit - 5) + ' al ' + limit + ' de un total de ' + taskNumber);
            var content = "";
            var categoriaStr = "";
            for (i = 0; i < resp['taskArray'].length; i++) {
                // Obtener HTML con los badges de las caraterísticas de la tarea iterada
                var categoria = resp['taskArray'][i]['category'].split(',');
                for (j = 0; j < categoria.length; j++) {
                    categoriaStr += '<span class="badge badge-primary">' + categoria[j] + '</span> &nbsp;';
                }
                // Construir fila de cada tarea
                content += '' +
                    '<tr class="text-center">' +
                    '   <td>' + resp['taskArray'][i]['name'] + '</td>' +
                    '   <td>' + categoriaStr + '</td>' +
                    '   <td><a href="#" title="Editar" data-id="' + resp['taskArray'][i]['id'] +
                    '" class="btn btn-link btn-xs js-btnEdit"><i class="fa fa-trash text-danger"></i>' +
                    '</a></td>' +
                    '</tr>' +
                    '';
                categoriaStr = "";
            }
            $("#tableContent").html(content);
        },
        error: function (err) {
            var content = '<tr><th colspan="3" class="text-center">' + err.responseJSON.message + '</th></tr>';
            $("#tableContent").html(content);
            $("#showingRegisters").html('Mostrando 0 registros');
        }
    });
}

function loadCategories() {
    //Cargar categorías
    $.ajax({
        url: 'controller/category/get.php',
        type: 'GET',
        success: function (resp) {
            var content = "";
            for (i = 0; i < resp.length; i++) { // Construir HTML (check) de cada categoría para añadirla al formulario
                content += '' +
                    '<div class="form-check">\n' +
                    '    <input type="checkbox" class="form-check-input" name="category[' + resp[i]["id"] +
                    ']" id="category' + resp[i]["id"] + '">\n' +
                    '    <label class="form-check-label">' + resp[i]["name"] + '</label>\n' +
                    '</div>' +
                    '';
            }
            $("#checksCategories").html(content);
        },
        error: function (err) {
            var content = '<label>' + err.responseJSON.message + '</label>';
            $("#checksCategories").html(content);
        }
    });
}
