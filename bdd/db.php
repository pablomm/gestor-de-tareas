<?php

class Database
{
    private $_connection;
    private static $_instance; // Instancia singleton
    private $_host = "container_bdd";
    private $_username = "root";
    private $_password = "root";
    private $_database = "taskTest";

    // Obtenemos una instancia de la propia clase Database
    public static function getInstance()
    {
        if(!self::$_instance) // If no instance then make one
        {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    private function __construct()
    {
        $this->_connection = new mysqli($this->_host, $this->_username,$this->_password, $this->_database);
        // Error handling
        if(mysqli_connect_error())
        {
            trigger_error("Failed to conencto to MySQL: " . mysql_connect_error(),E_USER_ERROR);
        }
    }

    // Obtener conexión
    public function getConnection()
    {
        return $this->_connection;
    }

}
