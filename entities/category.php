<?php
require __DIR__."/../bdd/db.php";

class Category extends Database
{
    private $db;

    // Propiedades de categoría
    public $id;
    public $name;

    public function __construct()
    {
        $this->db = Database::getInstance()->getConnection();
    }

    // Función que obtiene toda las categorías
    public function getCategories()
    {
        $sql = "SELECT * FROM category";
        $result = $this->db->query($sql);

        return $result;
    }
}
