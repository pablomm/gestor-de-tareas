<?php
require __DIR__ . "/../bdd/db.php";

class Task extends Database
{
    private $db;

//  Propiedades de tarea
    public $id;
    public $name;
    public $categories = array();

    public function __construct()
    {
        $this->db = Database::getInstance()->getConnection();
    }

    // Obtener número total de tareas
    public function getNumberTasks()
    {
        $sql = "SELECT COUNT(*) As TaskNumber FROM task";
        $result = $this->db->query($sql);

        return $result;
    }

    // Obtener todas las tareas
    public function getTasks($limit = 5)
    {
        $limitStart = $limit - 5;
        $sql = "SELECT task.id, task.name, IFNULL(GROUP_CONCAT(category.name),'') AS categoryName
                FROM task
                LEFT JOIN task_category ON (task_category.id_task=task.id)
                LEFT JOIN category ON (category.id=task_category.id_category)
                GROUP BY task.id
                ORDER BY task.name
                LIMIT " . $limitStart . ",5";
        $result = $this->db->query($sql);

        return $result;
    }

    // Añadir nueva tarea
    public function create()
    {
        $check = true;
        // Añado la tarea
        $sql = "INSERT INTO task (name) VALUES ('" . $this->name . "')";
        $this->db->query($sql);
        $this->id = $this->db->insert_id;

        if ($this->db->affected_rows == 0) {
            $check = false;
        }

        if ($this->categories) {
            // Recorro todas las categorías asociadas a la tarea y las voy añadiendo, sabiendo el id de la tarea
            foreach ($this->categories as $index => $value) {
                $sql2 = "INSERT INTO task_category (id_task, id_category) VALUES (" . $this->id . "," . $index . ")";
                $this->db->query($sql2);
                if ($this->db->affected_rows == 0) {
                    $check = false;
                }
            }
        }

        return $check;
    }

    // Borrar tarea
    public function delete()
    {
        $sql = "DELETE FROM task WHERE id = " . $this->id;
        $this->db->query($sql);

        // execute query
        if ($this->db->affected_rows > 0) {
            return true;
        }

        return false;
    }
}
