# Para iniciar el proyecto

1) Descargar docker de (https://www.docker.com/get-started)
2) Instalar npm en el caso que no esté instalado (comprobarlo con `npm -v`)
3) Descargar las dependencias establecidas en el fichero package.json con `npm install`
4) Desde la terminal, ir a la raiz del proyecto
5) Ejecutar: `docker-compose up --build -d`
- El mysql se iniciará en el puerto 3312
- El php en el puerto 8012 (Acceso en url: http://localhost:8012)
- No hay que preocuparse de cargar los datos iniciales, docker lo hace automáticamente, gracias a un fichero .sql que he creado (`.docker/CreateTable.sql`)

¡LISTO!

Nota: la primera vez puede tardar un poco en cargar los datos, dependiendo de la potencia del ordenador, por lo que la interfaz puede salir vacía. Intenta refrescar la página.

Nota: si no se cargan los datos en aproximadamente 1 min, se deberá a que el docker ha guardado la útima configuración y no ejecutará automáticamente el script (problema propio del docker), para resolverlo hacer lo siguiente por orden en la terminal:
1) `docker-compose stop`
2) `docker system prune`
3) Eliminar la carpeta `.docker/db_files` del proyecto, en caso de que exista.
4) `docker-compose up --build -d`
