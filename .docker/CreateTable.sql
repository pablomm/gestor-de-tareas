CREATE TABLE category (
  id INT(10) NOT NULL AUTO_INCREMENT,
  name VARCHAR(100) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=INNODB;

INSERT INTO category (id, name) VALUES ('1', 'PHP');
INSERT INTO category (id, name) VALUES ('2', 'JavaScript');
INSERT INTO category (id, name) VALUES ('3', 'CSS');

CREATE TABLE task (
  id INT(10) NOT NULL AUTO_INCREMENT,
  name VARCHAR(100) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=INNODB;

INSERT INTO task (id, name) VALUES ('1', 'Realizar backup de bdd');
INSERT INTO task (id, name) VALUES ('2', 'Funcion Ajax');

CREATE TABLE task_category (
  id_task INT(10) NOT NULL,
  id_category INT(10) NOT NULL,
  KEY id_task (id_task),
  KEY id_category (id_category),
  PRIMARY KEY (id_task,id_category),
  CONSTRAINT task_category_ibfk_1 FOREIGN KEY (id_task) REFERENCES task (id) ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT task_category_ibfk_2 FOREIGN KEY (id_category) REFERENCES category (id) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE=INNODB;

INSERT INTO task_category (id_task, id_category) VALUES (1,1);
INSERT INTO task_category (id_task, id_category) VALUES (2,1);
INSERT INTO task_category (id_task, id_category) VALUES (2,2);
