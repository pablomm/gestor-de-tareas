<?php
header("Content-Type: application/json; charset=UTF-8"); //Indica que se devolverá un JSON

require '../../entities/category.php';

$category = new Category(); // Crear el objeto categoría

$res = $category->getCategories(); // Llamar a la función de obtener categorías
$num = $res->num_rows; // Número de categorías devueltas

if ($num > 0) {
    $categoryArray = array();
    $categoriesArray = array();

    // Recorremos todas las categorías obtenidas
    while ($row = $res->fetch_assoc()) {
        $categoryArray = array(
            "id" => $row['id'],
            "name" => $row['name']
        );

        array_push($categoriesArray, $categoryArray);
    }

    // Devolvemos el array de categorías
    http_response_code(200);
    echo json_encode($categoriesArray);

} else {
    // No hay categorías
    http_response_code(404);
    echo json_encode(
        array("message" => "No hay categorías")
    );
}
