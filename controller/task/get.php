<?php
header("Content-Type: application/json; charset=UTF-8"); //Indica que se devolverá un JSON
require '../../entities/task.php';

$task = new Task(); // Crear el objeto tarea

$num = $task->getNumberTasks(); // Llamamos a la función de obtener tareas
$taskNumber = intval($num->fetch_assoc()['TaskNumber']); // Cantidad de tareas totales


// Recibimos limite para la paginación y realizamos ciertas comprobaciones
$limit = $_POST['limit'];
if ($limit == 0) {
    $limit = 5;
}
if ($limit >= ($taskNumber + 5)) {
    $limit = $taskNumber;
}

$res = $task->getTasks($limit); // Obtenemos todas las tareas
$numRows = $res->num_rows; // Número total de tareas

if ($numRows > 0) {
    $taskArray = array();
    $tasksArray = array();

    // Recorremos todas las tareas
    while ($row = $res->fetch_assoc()) {
        $taskArray = array(
            "id" => $row['id'],
            "name" => $row['name'],
            "category" => $row['categoryName']
        );
        array_push($tasksArray, $taskArray);
    }

    // Devolvemos el array de tareas
    http_response_code(200);
    echo json_encode(array("taskArray" => $tasksArray, "taskNumber" => $taskNumber));

} else {
    // No hay tareas
    http_response_code(404);
    echo json_encode(
        array("message" => "No hay tareas")
    );
}
