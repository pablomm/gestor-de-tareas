<?php
header("Content-Type: application/json; charset=UTF-8"); //Indica que se devolverá un JSON
include_once '../../entities/task.php';

$task = new Task(); // Crear el objeto tarea

$data = $_POST; // Datos recibidos por POST

// Otra comprobación de que el nombre de la tarea no sea vacío
if ($data['taskName'] != "") {

    // Rellenamos el objeto
    $task->name = $data['taskName'];
    $task->categories = $data['category'];

    // Llamamos a la función de crear tarea
    if ($task->create()) {
        // Tarea creada correctamente
        http_response_code(201);
        echo json_encode(array("message" => "Tarea creada"));
    }
    else {
        // Error en la creación
        http_response_code(503);
        echo json_encode(array("message" => "Error al crear la tarea"));
    }
} else {
    // No se han pasado todos los parámetros
    http_response_code(400);
    echo json_encode(array("message" => "Falta información"));
}
