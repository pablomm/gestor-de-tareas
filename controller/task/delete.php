<?php
header("Content-Type: application/json; charset=UTF-8"); //Indica que se devolverá un JSON
include_once '../../entities/task.php';

$task = new Task(); // Crear el objeto tarea

$task->id = $_POST['id']; // Dato recibido por POST

// Llamamos a la función de eliminar tarea
if ($task->delete()) {
    // Tarea eliminada correctamente
    http_response_code(200);
    echo json_encode(array("message" => "Tarea eliminada"));
}
else {
    // Ha ocurrido algún error
    http_response_code(503);
    echo json_encode(array("message" => "No se ha podido elimar la tarea"));
}
